<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、インストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さずにこのファイルを "wp-config.php" という名前でコピーして
 * 直接編集して値を入力してもかまいません。
 *
 * このファイルは、以下の設定を含みます。
 *
 * * MySQL 設定
 * * 秘密鍵
 * * データベーステーブル接頭辞
 * * ABSPATH
 *
 * @link http://wpdocs.sourceforge.jp/wp-config.php_%E3%81%AE%E7%B7%A8%E9%9B%86
 *
 * @package WordPress
 */

// 注意: 
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.sourceforge.jp/Codex:%E8%AB%87%E8%A9%B1%E5%AE%A4 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define('DB_NAME', 'ae111m5yj3_wordpress');

/** MySQL データベースのユーザー名 */
define('DB_USER', 'ae111m5yj3');

/** MySQL データベースのパスワード */
define('DB_PASSWORD', 'avKVtfaU');

/** MySQL のホスト名 */
define('DB_HOST', '127.0.0.1');

/** データベースのテーブルを作成する際のデータベースの文字セット */
define('DB_CHARSET', 'utf8mb4');

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define('DB_COLLATE', '');

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Fs^?B[x/  }$>|tI?[fisjAZ_|n%}AO~ I~|d&nWYPAwn9&SFiuA.|x7HBL(+F)<');
define('SECURE_AUTH_KEY',  '~nm53|J!|f<BcZg73xT*6X|->O~f#<Ediy+(>->1.iDR.z0:d)s}`0UttQ`l&;:J');
define('LOGGED_IN_KEY',    '@ATmKA !y74pl-$_E_dWS-s|silHOjMP_~@!a%f-RA]Xy@Xi+za5r=h>DIBMY~V.');
define('NONCE_KEY',        'beQA@C}wG7!G,8e{[K;|d@`bkuI+DFa+M<|~>rg^1<02f-`!e+|(:R){h]#11-6s');
define('AUTH_SALT',        '7{Lz5!t:Rm|;|)aGxb8{G-XvY]H8]i]qwha+Kg_/^UHK`D2>apdE./HRv.SCza-v');
define('SECURE_AUTH_SALT', 'h/PJ5bz{Npv^u$KC7b#MRn~-mE;0TvZFLIa!RlP9^,qI[_Au_,P]9y&t:>kztBEv');
define('LOGGED_IN_SALT',   'E4Gz1V#CH4iQ^|J{e@))v1x~B[++s,Si1u0-!aZU)Y)D`H]CUG6Ia]xa(:Wy^jRa');
define('NONCE_SALT',       '<PrBU:)_|{}2v<,Pf,~k%h`h1Uv~hm$Toj|Af$WB:]q2T+07,LD]uO|h9X4o^fL=');

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix  = 'wp_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 *
 * その他のデバッグに利用できる定数については Codex をご覧ください。
 *
 * @link http://wpdocs.osdn.jp/WordPress%E3%81%A7%E3%81%AE%E3%83%87%E3%83%90%E3%83%83%E3%82%B0
 */
define('WP_DEBUG', false);

/* 編集が必要なのはここまでです ! WordPress でブログをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
