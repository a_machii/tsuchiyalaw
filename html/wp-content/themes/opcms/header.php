<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<?php
  global $this_page_title;
  global $this_page_keywd;
  global $this_page_desc;
  global $this_page_slug;

  //固定ページの親ページ・子ページ
  $post_data = get_post($post->post_parent);
  $parent_title = $post_data->post_title;
  $parent_slug = $post_data->post_name;
?>
  <head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="target-densitydpi=device-dpi, width=1200, maximum-scale=1.0, user-scalable=yes">
    <meta name="format-detection" content="telephone=no,address=no,email=no">
    <meta name="description" content="<?php if($this_page_desc) : echo $this_page_desc. ' - '; elseif($this_page_title) : echo $this_page_title. ' - '; endif; ?>解雇・残業代請求等の労働問題、過払い金請求や遺産相続など幅広く対応できます。数多くの案件を経験し解決してきた当事務所に是非ご相談ください。">
    <meta name="keywords" content="<?php if($this_page_keywd) : echo $this_page_keywd. ','; elseif($this_page_title) : echo $this_page_title. ','; endif; ?>弁護士,法律,相談,請求,借金,過払い金,残業代,労働問題,交通事故,遺産相続" >
    <meta property="og:title" content="土谷総合法律事務所">
    <meta property="og:description" content="<?php if($this_page_desc) : echo $this_page_desc. ' - '; elseif($this_page_title) : echo $this_page_title. ' - '; endif; ?>解雇・残業代請求等の労働問題、過払い金請求や遺産相続など幅広く対応できます。数多くの案件を経験し解決してきた当事務所に是非ご相談ください。">
    <meta property="og:type" content="website">
    <meta property="og:url" content="<?php echo home_url('/'); ?><?php if($this_page_slug) : echo $this_page_slug; endif; ?>">
    <meta property="og:image" content="">
    <meta property="og:site_name" content="土谷総合法律事務所">
    <title>土谷総合法律事務所</title>
    <link rel="stylesheet" href="<?php echo home_url('/'); ?>assets/css/style.css">
    <!--[if lt IE 9]><script src="/javascripts/html5shiv.js"></script><![endif]-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo home_url('/'); ?>assets/js/smoothScrollEx.js"></script>
    <script type="text/javascript" src="<?php echo home_url('/'); ?>assets/js/rollover/opacity-rollover2.1.js"></script>
    <script type="text/javascript" src="<?php echo home_url('/'); ?>assets/js/rollover/rollover.js"></script>
<?php if(is_home()): ?>
    <script type="text/javascript" src="<?php echo home_url('/'); ?>assets/js/jquery.bxslider.min.js"></script>
<?php endif; ?>

    <!-- ロールオーバーの記述-->
    <script>
      $(document).ready(function() {
        $('.ophv').opOver(1.0, 0.6, 200, 200);
      });
    </script>

<?php if(is_home()): ?>
    <!-- スライダーの記述-->
    <script>
      $(function(){
        var slide = $('#slider ul').bxSlider({
          slideWidth  : 1100, //一枚あたりの長さ
          slideMargin : 0,
          controls    : false,
          pager       : true,
          auto        : true,
          minSlides   : 3,
          maxSlides   : 3,
          moveSlides  : 1,
          speed       : 1000,
          pause       : 5000,
          onSlideAfter: function(){
            slide.startAuto();
          }
        });
      });
    </script>
<?php endif; ?>
<?php wp_head(); ?>
  </head>

  <body>
    <header>
      <div id="header">
        <div class="inner">
          <h1>借金問題・労働問題・離婚問題でお悩みなら東京都千代田区の土谷総合法律事務所へ</h1>
          <p class="logo left"><a href="<?php echo home_url('/'); ?>"><img src="<?php echo home_url('/'); ?>assets/img/logo.gif" width="328" height="51" alt="土谷総合法律事務所"></a></p>
          <div class="unit right"><img src="<?php echo home_url('/'); ?>assets/img/tel.gif" width="248" height="48" alt="電話番号" class="left"><a href="<?php echo home_url('/'); ?>contact"><img src="<?php echo home_url('/'); ?>assets/img/rsvbtn_n.gif" width="230" height="52" class="right"></a></div><!-- .unit -->
        </div><!-- .inner -->
      </div><!-- #header -->

      <nav>
        <div id="gnav">
          <div class="inner">
            <ul>
              <li><a href="<?php echo home_url('/'); ?>"><img src="<?php echo home_url('/'); ?>assets/img/gnav01_n.png" width="142" height="70" alt="トップページ"></a></li>
              <li><a href="<?php echo home_url('/'); ?>service"><img src="<?php echo home_url('/'); ?>assets/img/gnav02_n.png" width="143" height="70" alt="業務のご案内"></a></li>
              <li><a href="<?php echo home_url('/'); ?>lawyer"><img src="<?php echo home_url('/'); ?>assets/img/gnav03_n.png" width="143" height="70" alt="弁護士紹介"></a></li>
              <li><a href="<?php echo home_url('/'); ?>flow"><img src="<?php echo home_url('/'); ?>assets/img/gnav04_n.png" width="143" height="70" alt="ご相談の流れ"></a></li>
              <li><a href="<?php echo home_url('/'); ?>case"><img src="<?php echo home_url('/'); ?>assets/img/gnav05_n.png" width="143" height="70"  alt="事例紹介"></a></li>
              <li><a href="<?php echo home_url('/'); ?>faq"><img src="<?php echo home_url('/'); ?>assets/img/gnav06_n.png" width="143" height="70" alt="よくあるご質問"></a></li>
              <li><a href="<?php echo home_url('/'); ?>office"><img src="<?php echo home_url('/'); ?>assets/img/gnav07_n.png" width="143" height="70" alt="事務所紹介"></a></li>
            </ul>
          </div><!-- .inner -->
        </div><!-- #gnav -->
      </nav>

<?php if(is_home()): ?>
      <div id="slider">
        <div class="slider_inner">
          <ul>
            <li><img src="<?php echo home_url('/'); ?>assets/img/top/mainimg03.jpg" width="1100" height="380" alt="土谷総合法律事務所の写真"></li>
            <li><img src="<?php echo home_url('/'); ?>assets/img/top/mainimg01.jpg" width="1100" height="380" alt="土谷総合法律事務所の写真"></li>
            <li><img src="<?php echo home_url('/'); ?>assets/img/top/mainimg02.jpg" width="1100" height="380" alt="土谷総合法律事務所の写真"></li>
          </ul>
          <div id="slideFilterL"></div>
          <div id="slideFilterR"></div>
        </div><!-- .slider_inner -->
      </div><!-- #slider -->
<?php else: ?>
      <div id="low_mimg">
        <div class="inner">
  <?php if (is_page() && $post->post_parent): ?>
          <p class="page_ttl right"><img src="<?php echo home_url('/'); ?>assets/img/<?php echo $parent_slug; ?>/page_ttl.png" alt="<?php echo $parent_title; ?>"></p>
  <?php else: ?>
          <p class="page_ttl right"><img src="<?php echo home_url('/'); ?>assets/img/<?php echo $this_page_slug; ?>/page_ttl.png" alt="<?php echo $this_page_title; ?>"></p>
  <?php endif; ?>
        </div><!-- .inner -->
      </div><!-- #low_mimg -->
<?php endif; ?>
    </header>

<?php if(!is_home()): ?>
    <nav>
      <div id="breadcrumb">
        <div class="inner">
          <ul>
  <?php if (is_page() && $post->post_parent): ?>
            <li><a href="<?php echo home_url('/'); ?>">トップページ</a>&nbsp;&gt;&nbsp;</li>
            <li><a href="<?php echo get_page_link($post->post_parent); ?>"><?php echo $parent_title; ?></a>&nbsp;&gt;&nbsp;</li>
            <li><?php echo $this_page_title; ?></li>
  <?php else: ?>
            <li><a href="<?php echo home_url('/'); ?>">トップページ</a>&nbsp;&gt;&nbsp;</li>
            <li><?php echo $this_page_title; ?></li>
  <?php endif; ?>
          </ul>
        </div><!-- .inner -->
      </div><!-- #breadcrumb -->
    </nav>
<?php endif; ?>