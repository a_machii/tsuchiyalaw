<?php /* Template Name: page */ ?>
<?php
  $page_id = $post->ID; //xxxに 固定ページIDを入力
  $content = get_page($page_id);
  $this_page_title = $content->post_title;
  $this_page_slug = $content->post_name;
?>
<?php get_header(); ?>

    <div id="low" class="wrap">
      <div class="inner">
<?php get_sidebar(); ?>

        <main>
<?php if(is_page('service')): ?>
          <div id="<?php echo $this_page_slug; ?>" class="main right">
<?php elseif(is_page(array('debt'))): ?>
          <div id="<?php echo $this_page_slug; ?>" class="service_low main right">
<?php endif; ?>
<?php if(have_posts()): while(have_posts()): the_post(); ?>
<?php the_content(); ?>
<?php endwhile; endif; ?>

            <div class="ct_bnr"><a href="<?php echo home_url('/'); ?>contact"><img src="<?php echo home_url('/'); ?>assets/img/bnr_rsvbtn_n.gif" width="225" height="43" alt="無料相談のご予約はこちら"></a></div><!-- .ct_bnr -->
          </div><!-- #<?php echo $this_page_slug; ?> -->
        </main>
      </div><!-- .inner -->
    </div><!-- #wrap -->

<?php get_footer(); ?>