<?php get_header(); ?>

    <section>
      <div id="tsrv">
        <div class="tsrv_inner">
          <h2><img src="<?php echo home_url('/'); ?>assets/img/top/tsrv_ttl.gif" width="243" height="45" alt="取り扱い業務のご案内"></h2>
          <ul>
            <li><a href="<?php echo home_url('/'); ?>service/debt.html" class="ophv"><img src="<?php echo home_url('/'); ?>assets/img/top/tsrv01.jpg" width="210" height="300" alt="借金問題"></a></li>
            <li><a href="<?php echo home_url('/'); ?>service/work.html" class="ophv"><img src="<?php echo home_url('/'); ?>assets/img/top/tsrv02.jpg" width="210" height="300" alt="労働問題"></a></li>
            <li><a href="<?php echo home_url('/'); ?>service/divorce.html" class="ophv"><img src="<?php echo home_url('/'); ?>assets/img/top/tsrv03.jpg" width="210" height="300" alt="離婚問題"></a></li>
            <li><a href="<?php echo home_url('/'); ?>service/corporate-law.html" class="ophv"><img src="<?php echo home_url('/'); ?>assets/img/top/tsrv04.jpg" width="210" height="300" alt="企業法務"></a></li>
            <li><a href="<?php echo home_url('/'); ?>service/general-law.html" class="ophv"><img src="<?php echo home_url('/'); ?>assets/img/top/tsrv05.jpg" width="210" height="300" alt="一般法律"></a></li>
          </ul>
        </div><!-- .tsrv_inner -->
      </div><!-- tsrv -->
    </section>

    <div id="top" class="wrap">
      <div class="inner">
<?php get_sidebar(); ?>

        <main>
          <div id="tmain" class="main right">
            <div id="section01" class="section">
              <p>この度は、土谷総合法律事務所のホームページをご覧いただきありがとうございます。<br>当事務所へご連絡いただくお客様の中には、「法律に詳しくないから主張をどうやってまとめたらいいかわからない」と言われる方がいます。<br>言わば私たち法律に詳しい専門家が、依頼者の主張をうまくまとめて事件を有利に進めるのが弁護士の仕事です。日本は幸いにも法治国家です。<br>法律でつまずいたら、法律の番人へ、まずはお気軽にご相談くださいませ。</p>
            </div><!-- #section01 -->

            <section>
              <div id="section02" class="section">
                <h2 class="h2ttl">土谷総合法律事務所の３つのお約束</h2>
                <p class="txt">当事務所では、ご依頼者様が安心してご利用いただけるサービスをご提供しております。</p>
                <ul>
                  <li>
                    <h3>初回相談無料</h3>
                    <div class="unit">
                      <p class="txt02">ご依頼者様のお話しをじっくりお聞きするため、初回のご相談は無料です。時間をかけてゆっくり状況をご説明ください。</p><img src="<?php echo home_url('/'); ?>assets/img/top/sec02_img01.jpg" width="210" height="105" alt="">
                    </div>
                  </li>
                  <li>
                    <h3>豊富な経験・実績</h3>
                    <div class="unit">
                      <p class="txt02">弁護士歴30年以上の経験と知識を活かし、ご依頼者様に最良・最善な解決方法をご提案いたします。安心してご依頼ください。</p><img src="<?php echo home_url('/'); ?>assets/img/top/sec02_img02.jpg" width="210" height="105" alt="">
                    </div>
                  </li>
                  <li>
                    <h3>迅速・丁寧なサポート</h3>
                    <div class="unit">
                      <p class="txt02">様々な案件を解決してきた弁護士が迅速に対応いたします。ご依頼者様に分かりやすい説明を心掛けておりますのでお気軽にご相談ください。</p><img src="<?php echo home_url('/'); ?>assets/img/top/sec02_img03.jpg" width="210" height="105" alt="">
                    </div>
                  </li>
                </ul>
              </div><!-- #section02 -->
            </section>

            <section>
              <div id="section03" class="section">
                <h2 class="h2ttl">事務所からのお知らせ<span class="list"><a href="<?php echo home_url('/'); ?>info">一覧はこちら &gt;</a></span></h2>
                <dl>
                  <dt>2016.09.28</dt>
                  <dd><a href="<?php echo home_url('/'); ?>info">ホームページをリニューアルしました</a></dd>
                </dl>
              </div><!-- #section03 -->
              <div class="ct_bnr"><img src="<?php echo home_url('/'); ?>assets/img/bnr_rsvbtn_n.gif" width="225" height="43" alt="無料相談のご予約はこちら"></div><!-- .ct_bnr -->
            </section>
          </div><!-- #main -->
        </main>
      </div><!-- .inner -->
    </div><!-- #wrap -->

<?php get_footer(); ?>