    <footer>
      <div id="footer">
        <div class="inner">
          <div class="unit01 left">
            <p class="flogo"><a href="<?php echo home_url('/'); ?>"><img src="<?php echo home_url('/'); ?>assets/img/flogo.png" width="282" height="45" alt="土谷総合法律事務所"></a></p>
            <dl>
              <dt>土谷総合法律事務所</dt>
              <dd>〒101-0051</dd>
              <dd>東京都千代田区神田神保町3-5　ニュー徳栄ビル4階</dd>
              <dd>TEL：03-6272-5485 / FAX：03-6272-6433</dd>
            </dl>
          </div><!-- .unit01 -->

          <div class="unit02 right">
            <nav>
              <div class="fnav_area left">
                <ul id="fnav01" class="left">
                  <li><a href="<?php echo home_url('/'); ?>service" class="first">業務のご案内</a>
                    <ul>
                      <li><a href="<?php echo home_url('/'); ?>service/debt.html">借金問題</a></li>
                      <li><a href="<?php echo home_url('/'); ?>service/work.html">労働問題</a></li>
                      <li><a href="<?php echo home_url('/'); ?>service/divorce.html">離婚問題</a></li>
                      <li><a href="<?php echo home_url('/'); ?>service/corporate-law.html">企業法務</a></li>
                      <li><a href="<?php echo home_url('/'); ?>service/general-law.html">一般法律</a></li>
                    </ul>
                  </li>
                </ul>

                <ul id="fnav02" class="left">
                  <li><a href="<?php echo home_url('/'); ?>">トップページ</a></li>
                  <li><a href="<?php echo home_url('/'); ?>flow">ご相談の流れ</a></li>
                  <li><a href="<?php echo home_url('/'); ?>case">事例紹介</a></li>
                  <li><a href="<?php echo home_url('/'); ?>service/corporate-law.html#h302">顧問契約について</a></li>
                  <li><a href="<?php echo home_url('/'); ?>faq">よくあるご質問</a></li>
                  <li><a href="<?php echo home_url('/'); ?>lawyer">弁護士紹介</a></li>
                </ul>

                <ul id="fnav03" class="left">
                  <li><a href="<?php echo home_url('/'); ?>office">事務所紹介</a></li>
                  <li><a href="<?php echo home_url('/'); ?>office#section02">アクセスマップ</a></li>
                  <li><a href="<?php echo home_url('/'); ?>info">お知らせ</a></li>
                  <li><a href="<?php echo home_url('/'); ?>contact">お問い合わせ</a></li>
                  <li><a href="<?php echo home_url('/'); ?>contact#section03">個人情報保護方針</a></li>
                </ul>
              </div><!-- .fnav_area -->

              <p class="to_top right"><a href="#header" class="ophv"><img src="<?php echo home_url('/'); ?>assets/img/to_top.gif" width="45" height="45" alt="トップへ戻る"></a></p>
            </nav>
          </div><!-- .unit02 -->

          <p class="copyright">Copyright © Tsuchiya General Law Office All Rights Reserved.</p>
        </div><!-- .inner -->
      </div><!-- #footer -->
    </footer>
<?php wp_footer(); ?>
  </body>
</html>