        <aside>
          <div id="sidebar" class="left">
<?php if(is_page('service')): ?>
            <div class="side_list">
              <p><img src="<?php echo home_url('/'); ?>assets/img/service/slist_ttl.jpg" width="225" height="45" alt="業務のご案内"></p>
              <ul class="sl_inner">
                <li><a href="<?php echo home_url('/'); ?>service/debt">借金問題</a></li>
                <li><a href="<?php echo home_url('/'); ?>service/work">労働問題</a></li>
                <li><a href="<?php echo home_url('/'); ?>service/divorce">離婚問題</a></li>
                <li><a href="<?php echo home_url('/'); ?>service/corporate-law">企業法務</a></li>
                <li><a href="<?php echo home_url('/'); ?>service/general-law">一般法律</a></li>
              </ul>
            </div><!-- .side_list -->
<?php endif; ?>
            <div class="side_ct"><a href="<?php echo home_url('/'); ?>contact"><img src="<?php echo home_url('/'); ?>assets/img/side_rsvbtn_n.gif" width="200" height="40"  alt="無料相談のご予約はこちら"></a></div><!-- side_ct -->

            <ul class="link_list">
              <li><a href="<?php echo home_url('/'); ?>service" class="ophv"><img src="<?php echo home_url('/'); ?>assets/img/side_srv.jpg" width="217" height="69" alt="業務のご案内"></a></li>
              <li><a href="<?php echo home_url('/'); ?>service/corporate-law.html#h302" class="ophv"><img src="<?php echo home_url('/'); ?>assets/img/side_adv.jpg" width="217" height="69" alt="顧問契約について"></a></li>
              <li><a href="<?php echo home_url('/'); ?>faq" class="ophv"><img src="<?php echo home_url('/'); ?>assets/img/side_faq.jpg" width="217" height="69" alt="よくあるご質問"></a></li>
              <li><a href="<?php echo home_url('/'); ?>case" class="ophv"><img src="<?php echo home_url('/'); ?>assets/img/side_case.jpg" width="217" height="69" alt="解決事例紹介"></a></li>
            </ul>

            <p class="fb"><a href="" class="ophv"><img src="<?php echo home_url('/'); ?>assets/img/side_fb.jpg" width="225" height="75" alt="facebookページ"></a></p>

            <dl class="side_off">
              <dt>土谷総合法律事務所</dt>
              <dd>〒101-0051</dd>
              <dd>東京都千代田区神田神保町3-5</dd>
              <dd>ニュー徳栄ビル4階</dd>
              <dd>TEL：03-6272-5485</dd>
              <dd>FAX：03-6272-6433</dd>
              <dd class="gmap"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3240.2449722491547!2d139.75181286525918!3d35.695588830190935!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x60188c149731c13b%3A0x91fe2b3be040eef3!2z44CSMTAxLTAwNTEg5p2x5Lqs6YO95Y2D5Luj55Sw5Yy656We55Sw56We5L-d55S677yT5LiB55uuIOODi-ODpeODvOW-s-aghOODk-ODqw!5e0!3m2!1sja!2sjp!4v1472622144363" width="203" height="203" frameborder="0" style="border:0" allowfullscreen></iframe></dd>
              <dd><a href="<?php echo home_url('/'); ?>office">事務所概要はこちら &gt;</a></dd>
            </dl>
          </div><!-- #sidebar -->
        </aside>
