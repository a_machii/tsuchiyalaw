<?php

function call_thissiteurl() {
    return site_url('/');
}
add_shortcode('thissiteurl', 'call_thissiteurl');

function remove_footer_admin () {
  echo 'お問い合わせは<a href="http://www.officepartner.jp/contact/" target="_blank">オフィスパートナー株式会社</a>まで';
}
add_filter('admin_footer_text', 'remove_footer_admin');

if (!current_user_can('administrator')) {
  add_filter('pre_site_transient_update_core', create_function('$a', "return null;"));
}

if (!current_user_can('edit_users')) {
  function remove_menus () {
    global $menu;
    $restricted = array(
      __('リンク'),
      __('ツール'),
      __('コメント'),
      __('プロフィール')
      );
    end ($menu);
    while (prev($menu)){
      $value = explode(' ',$menu[key($menu)][0]);
      if(in_array($value[0] != NULL?$value[0]:"" , $restricted)){
        unset($menu[key($menu)]);
      }
    }
  }
  add_action('admin_menu', 'remove_menus');
}

function example_remove_dashboard_widgets() {
  global $wp_meta_boxes;
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']); // 現在の状況
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']); // 最近のコメント
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']); // 被リンク
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']); // プラグイン
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']); // クイック投稿
  //unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']); // 最近の下書き
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']); // WordPressブログ
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']); // WordPressフォーラム
}
add_action('wp_dashboard_setup', 'example_remove_dashboard_widgets');

add_action( 'wp_before_admin_bar_render', 'hide_before_admin_bar_render' );
function hide_before_admin_bar_render() {
  global $wp_admin_bar;
  $wp_admin_bar->remove_menu( 'wp-logo' );
}

// ウィジェット
//register_sidebar();

/* ログイン画面のロゴ変更
------------------------------------------------------------*/
function my_custom_login_logo() {
  echo '<style type="text/css">
  h1 a { background-image:url('.get_bloginfo('template_directory').'/images/logo-login.png) !important; }</style>';
  echo '
  <script type="text/javascript">

  </script>
  ';
}
add_action('login_head', 'my_custom_login_logo');

/* メニューの表示順番
------------------------------------------------------------*/
function custom_menu_order($menu_ord) {
  if (!$menu_ord) return true;
  return array(
    'index.php', // ダッシュボード
    'separator1', // 最初の区切り線
    'edit.php', // 投稿
    'edit.php?post_type=page', // 固定ページ
    'upload.php', // メディア
    'link-manager.php', // リンク
    'edit-comments.php', // コメント
    'separator2', // 二つ目の区切り線
    'themes.php', // 外観
    'plugins.php', // プラグイン
    'users.php', // ユーザー
    'tools.php', // ツール
    'options-general.php', // 設定
    'separator-last', // 最後の区切り線
  );
}
add_filter('custom_menu_order', 'custom_menu_order'); // Activate custom_menu_order
add_filter('menu_order', 'custom_menu_order');

/* メニューを非表示にする（管理者以外）
------------------------------------------------------------*/
function remove_menus02 () {
  if (!current_user_can('level_10')) { //level10以下のユーザーの場合メニューをunsetする
    remove_menu_page('wpcf7'); //Contact Form 7
    global $menu;
    unset($menu[5]); // 投稿
    unset($menu[20]); // 固定ページ
  }
}
add_action('admin_menu', 'remove_menus02');

/* wp_head()のいらないタグを削除
-------------------------------------------------------------*/
// 絵文字
function disable_emoji() {
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
  remove_action( 'wp_print_styles', 'print_emoji_styles' );
  remove_action( 'admin_print_styles', 'print_emoji_styles' );
  remove_action( 'wp_head', 'wp_shortlink_wp_head');
  remove_action('wp_head', 'rel_canonical');
  remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
  remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
  remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
}
add_action( 'init', 'disable_emoji' );

// Embed
remove_action('wp_head','rest_output_link_wp_head');
remove_action('wp_head','wp_oembed_add_discovery_links');
remove_action('wp_head','wp_oembed_add_host_js');

// generator
remove_action('wp_head', 'wp_generator');

// EditURI
remove_action('wp_head', 'rsd_link');

// wlwmanifest
remove_action('wp_head', 'wlwmanifest_link');

/* 固定ページではビジュアルエディタを利用できないようにする
------------------------------------------------------------*/
function disable_visual_editor_in_page(){
  global $typenow;
  if( $typenow == 'page' ){
    add_filter('user_can_richedit', 'disable_visual_editor_filter');
  }
}

function disable_visual_editor_filter(){
  return false;
}
add_action( 'load-post.php', 'disable_visual_editor_in_page' );
add_action( 'load-post-new.php', 'disable_visual_editor_in_page' );

/* 自動整形を無効にする
-------------------------------------------------------------*/
add_filter('the_content', 'wpautop_filter', 9);
function wpautop_filter($content) {
  global $post;
  $remove_filter = false;
    $arr_types = array('page'); //自動整形を無効にする投稿タイプを記述
    $post_type = get_post_type( $post->ID );
    if (in_array($post_type, $arr_types)) $remove_filter = true;
    if ( $remove_filter ) {
      remove_filter('the_content', 'wpautop');
      remove_filter('the_excerpt', 'wpautop');
    }
  return $content;
}

/* カテゴリーの階層構造を正しく表示
------------------------------------------------------------*/
function lig_wp_category_terms_checklist_no_top( $args, $post_id = null ) {
  $args['checked_ontop'] = false;
  return $args;
}
add_action( 'wp_terms_checklist_args', 'lig_wp_category_terms_checklist_no_top' );

/* カスタム投稿の追加
------------------------------------------------------------*/
add_action( 'init', 'create_post_type' );
function create_post_type() {
  register_post_type('custompost',
    array(
      'label' => 'カスタム投稿',
      'description' => '',
      'public' => true,
      'show_ui' => true,
      'show_in_menu' => true,
      'capability_type' => 'post',
      'hierarchical' => false,
      'rewrite' => true,
      'query_var' => false,
      'has_archive' => true,
      'exclude_from_search' => false,
      'menu_position' => 20,
      'supports' => array('title','editor'),
      'taxonomies' => array('custompostcategory'),
      'labels' => array (
        'name' => 'カスタム投稿',
        'all_items' => 'カスタム投稿一覧'
        )
      )
    );

  register_taxonomy(
    'custompostcategory',
    'custompost',
    array(
      'hierarchical' => true,
      'label' => 'カスタム投稿カテゴリ',
      'show_ui' => true,
      'query_var' => true,
      'rewrite' => array('slug' => 'custompost'),
      'singular_label' => 'カスタム投稿カテゴリ'
      )
    );
}

/* カスタム分類アーカイブ用のリライトルールを追加する
------------------------------------------------------------*/
add_rewrite_rule('custompost/([^/]+)/page/([0-9]+)/?$', 'index.php?custompostcategory=$matches[1]&paged=$matches[2]', 'top'); //2ページ目以降用

/* pre get posts 設定
------------------------------------------------------------*/
function customize_main_query($query) {
  if ( is_admin() || ! $query->is_main_query() )
    return;

  if ( $query->is_post_type_archive( 'custompost' )|| $query->is_tax( 'custompostcategory' ) ) {
    $query->set( 'posts_per_page', '10' );
  }
}
add_action( 'pre_get_posts', 'customize_main_query' );

/* 画像サイズの変更
-------------------------------------------------------------*/
add_theme_support('post-thumbnails');
if ( function_exists( 'add_image_size' ) ) {
  add_image_size( 'thumb_name', width, height, true ); //(cropped)
}

/* pagenation
-------------------------------------------------------------*/
function pagination($pages = '', $range = 9){
  // $showitems数から何ページ分ページネーションを作成するか計算する
  $showitems = 1;    //($range * 2)+1;

  // 現在いる記事一覧ページの値を取得
  global $paged;
  if(empty($paged)) $paged = 1;

  // 存在する記事に対するページ総数の判断
  if($pages == ''){
    global $wp_query;
    $pages = $wp_query->max_num_pages;
      if(!$pages){
        $pages = 1;
       }
     }

  // $pagesが1で無かった場合＝記事がある程度存在する場合のページネーションの表示
  if(1 != $pages){
    echo "<div class=\"pager_area\">\n\t<ul>\n";
    //if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo; First</a>";

    // 一つ前に戻るボタン
    if($paged > 1 && $showitems < $pages) echo "\t\t<li class=\"ba\"><a href='".get_pagenum_link($paged - 1)."'>&lsaquo;</a></li>";

    // 現在のページ ループ部分
    for ($i=1; $i <= $pages; $i++){ //全記事一覧が1ページ以上ある
      if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems )){
        echo ($paged == $i)? "\t\t<li class=\"active\"><span class=\"current\">".$i."</span></li>\n":"\t\t<li><a href='".get_pagenum_link($i)."' class=\"inactive\">".$i."</a></li>\n";
      }
    }

    // 一つ進むボタン
    if ($paged < $pages && $showitems < $pages) echo "\t\t<li class=\"ba\"><a href=\"".get_pagenum_link($paged + 1)."\">&rsaquo;</a></li>\n";
    //if ($paged < $pages-1 && $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>Last &raquo;</a>";
    echo "\t</ul>\n</div><!-- /.pager_area -->\n";
  }
}

/* アーカイブ表記に「年」を追加
-------------------------------------------------------------*/
function my_archives_link($html){
  if(preg_match('/[0-9]+?<\/a>/', $html))
    $html = preg_replace('/([0-9]+?)<\/a>/', '$1年</a>', $html);
  if(preg_match('/title=[\'\"][0-9]+?[\'\"]/', $html))
    $html = preg_replace('/(title=[\'\"][0-9]+?)([\'\"])/', '$1年$2', $html);
  return $html;
}
add_filter('get_archives_link', 'my_archives_link', 10);

function is_parent_slug() {
  global $post;
  if ($post->post_parent) {
    $post_data = get_post($post->post_parent);
    return $post_data->post_name;
  }
}


?>