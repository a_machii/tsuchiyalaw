var gulp = require("gulp");
var sass = require("gulp-sass");
var autoprefixer = require("gulp-autoprefixer");
var uglify = require("gulp-uglify");
var browser = require("browser-sync");
var plumber = require("gulp-plumber");
var jade = require('gulp-jade');

gulp.task("server", function() {
  browser({
    server: {
      baseDir: "./_html"
    }
  });
});

gulp.task("sass", function() {
  gulp.src("_html/assets/sass/**/*scss")
    .pipe(plumber())
    .pipe(sass({outputStyle: 'expanded'}))
    .pipe(autoprefixer())
    .pipe(gulp.dest("./_html/assets/css"))
    .pipe(browser.reload({stream:true}))
});

gulp.task("js", function() {
  gulp.src(["_html/assets/js/**/*.js","!_html/assets/js/min/**/*.js"])
    .pipe(uglify())
    .pipe(gulp.dest("./_html/assets/js/min"))
    .pipe(browser.reload({stream:true}))
});

gulp.task('jade', function(){
  gulp.src(["jade/**/*.jade","!jade/template/*.jade"])
    .pipe(plumber())
    .pipe(jade({
      pretty: true
    }))
    .pipe(gulp.dest("./_html/"))
    .pipe(browser.reload({stream:true}))
});

gulp.task("_html", function() {
  gulp.src(["_html/**/*._html"])
    .pipe(browser.reload({stream:true}))
});

gulp.task("default",['server'], function() {
  gulp.watch(["_html/assets/js/**/*.js","!_html/assets/js/min/**/*.js"],["js"]);
  gulp.watch("_html/assets/sass/**/*.scss",["sass"]);
  gulp.watch(["jade/**/*.jade", '!jade/template/*.jade'],["jade"]);
  gulp.watch("_html/**/*._html",["_html"]);
});